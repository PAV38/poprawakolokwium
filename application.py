import os
import uuid
from flask import *
from flaskext.couchdb import CouchDBManager
import requests
import datetime
from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

COUCHDB_SERVER = 'http://194.29.175.241:5984'
COUCHDB_DATABASE = 'chat1'
SECRET_KEY = 'development key'

app = Flask(__name__)
manager = CouchDBManager()
manager.setup(app)
app.config.from_object(__name__)
engine = create_engine('sqlite:///test.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))
Base = declarative_base()


users = {'admin':True}

def init_db():
    Base.metadata.create_all(bind=engine)


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    postText = Column(String(50), unique=False)
    postAuthor = Column(String(120), unique=False)
    postDate = Column(String(120), unique=False)
    def __init__(self, postText=None, postAuthor=None, postDate=None):
        self.postText = postText
        self.postAuthor = postAuthor
        self.postDate = postDate
    def __repr__(self):
        return '<Post %r>' % (self.postText)

@app.route('/register')
def registerApp():
    try:
        dataToRegisterApplication = {'host' : 'http://shrouded-peak-8991.herokuapp.com', 'active' : True, 'delivery' : '/add' }
        g.couch['shrouded-peak-8991'] = dataToRegisterApplication
        g.couch.save(dataToRegisterApplication)
        return 'Zarejestrowano aplikacje w couch DB pod kluczem: limitless-island-4957'
    except:
        return 'Ta aplikacja zostala juz zarejestrowana i jest aktywna!'

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/posts')
def posts():
    init_db()
    posts = db_session.query(Post).all()
    return render_template('posts.html', messages=posts)

@app.route('/add', methods=['GET', 'POST'])
def add():
    messageValue = ''
    if request.method == 'POST':
        messageValue = request.form['message']
        try:
            newPost = Post(messageValue, session['loggedUser'], str(datetime.datetime.now()))
            db_session.add(newPost)
            db_session.commit()
        except:
            render_template('error.html')
    try:
        tabWithLinks = getActiveLinks()
        sendedMessage = {"user": session['loggedUser'], "message":messageValue, "timestamp":str(datetime.datetime.time()), "id":str(uuid.uuid4())}
        for link in tabWithLinks:
            try:
                sessionObj = requests.session()
                sessionObj.post(link, data=sendedMessage, verify=False)
            except:
                print ('nieudane wyslanie na adres:' + link)
    except:
        pass
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def Login():
    if request.method == 'POST':
        result = doLogin(request.form['loginValue'])
        if result == True:
            return render_template('index.html')
        else:
            return render_template('error.html')
    else:
        return render_template('loginForm.html')


@app.route('/changeNick', methods=['GET', 'POST'])
def changeNick():
    if request.method == 'POST':
        logOut()
        doLogin(request.form['loginValue'])
    else:
        return render_template('changeNick.html')
    return render_template('index.html')



@app.route('/logout')
def logOut():
    users[session['loggedUser']] = False
    session.pop('login', None)
    session.pop('loggedUser', None)
    return render_template('index.html')


def getAllMessagesFromFile():
    lines = ''
    fileToOpen = open(os.path.join('static', 'messages.txt'), "r")
    lines = fileToOpen.readlines()
    fileToOpen.close()
    return lines

def adddMessageToFile(parameter):
    lines = ''
    fileToOpen = open(os.path.join('static', 'messages.txt'), "r")
    lines = fileToOpen.readlines()
    fileToOpen.close()
    fileToOpen = open(os.path.join('static', 'messages.txt'), "w")
    for x in lines:
        fileToOpen.write(x)
    fileToOpen.write(parameter)
    fileToOpen.write('\n')
    fileToOpen.close()

def doLogin(userLogin):
    jestZalogowany = False
    if users.has_key(userLogin):
        if users[userLogin] == True:
            jestZalogowany = True
            return False
    else:
        users[userLogin] = True
    if userLogin != None and jestZalogowany == False:
        session['login'] = True
        session['loggedUser'] = userLogin
        users[userLogin] = True
        return True
    else:
        return False

def getActiveLinks():
    listWithLinks = []
    url = "http://194.29.175.241:5984/chat/_design/utils/_view/list_active"
    session = requests.session()
    odp = session.get(url)
    slownik = odp.json()
    i = 0
    for x in slownik["rows"]:
        listWithLinks.append(slownik["rows"][i]['value']['host'] + slownik["rows"][i]['value']['delivery'])
        i = i+1
    return listWithLinks


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('PORT', 5000)), debug=True)
    #app.run(debug=True)
